# PuppetTerraformIntegration

#Prerequisites
- Install Terraform
- Install AWS CLI

#Installation Steps
- Configure AWS CLI(create service user and create key and authenticate using 'aws configure')
- create keypair on AWS EC2 for authentication and EC2 instance creation
- Initialize Terraform (terraform init)
- Create Terraform Configuration
- Integrate Puppet
- Validate, Plan and Apply terraform

#Integration between Terraform and Puppet
- Use puppet provisioner in Terraform
- Provisioner "remote-exec"
        - Install Puppet Modules
        - Define Puppet Manifests
        - Apply Puppet Manifests
- Terraform apply
- Monitor and Troubleshoot

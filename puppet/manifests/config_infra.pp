# Puppet Manifests for configuring the provisioned infrastructure

# Example: Install Apache and start the service
class apache {
  package { 'apache2':
    ensure => installed,
  }

  service { 'apache2':
    ensure => running,
    enable => true,
  }
}

include apache